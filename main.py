# Подключение библиотек
import hashlib
import time
import csv

t = int(round(time.time() * 1000))  # Начало программы в миллисекундах
fiob = b'Garinski K.S.'  # Строка в битах
fio = 'Garinskih K.S.'
# ---------------------------------------------
my_file = open("fio.txt", 'wb+')  # Проверяем имеется ли файл с названием fio
# если нет создаем если имеется открываем в битах
my_file.write(fio.encode('utf-8'))  # записываем строку в файл
my_file.close()  # закрывем файл
my_file = open("fio.txt", 'rb+')  # Открываем файл для чтения
ride_my_file = my_file.read()
# ---------------------------------------------
hash_object = hashlib.md5(fio.encode('utf-8'))  # Переводим в биты строку и получаем хеш
fio1 = fio  # Данные для таблицы
hesh1 = hash_object.hexdigest()  # Данные для таблицы
print(f"Хеш строки------------------------------------------------------------------>{hash_object.hexdigest()}")
time1 = int(round(time.time() * 1000)) - t  # Данные для таблицы
# ---------------------------------------------
hash_object = hashlib.md5(ride_my_file)  # Переводим в биты файл и получаем хеш
fio2 = ride_my_file.decode('utf-8')  # Данные для таблицы
hesh2 = hash_object.hexdigest()  # Данные для таблицы
print(f"Хеш файла------------------------------------------------------------------->{hash_object.hexdigest()}")
time2 = int(round(time.time() * 1000)) - t  # Данные для таблицы
# ---------------------------------------------
hash_object = hashlib.md5(fio[1:-1].encode('utf-8'))  # Переводим в биты строку, убераем первую
# буква и получаем хеш
fio3 = fio[1:-1]  # Данные для таблицы
hesh3 = hash_object.hexdigest()  # Данные для таблицы
print(f"Хеш с убранной первой буквой({fio[1:-1]})---------------------------------->{hash_object.hexdigest()}")
time3 = int(round(time.time() * 1000)) - t  # Данные для таблицы
# ---------------------------------------------
fio = list(fio)  # Разбиваем строку в 'массив'
fio[0] = chr(ord(fio[0]) ^ 1)  # Меняем первый бит, беровй буквы
fio = ''.join(fio)  # возвращаем массив в страку
hash_object = hashlib.md5(fio.encode('utf-8'))  # Переводим в биты строку и получаем хеш
fio4 = fio  # Данные для таблицы
hesh4 = hash_object.hexdigest()  # Данные для таблицы
print(f"Хеш с заменной бита(при замене меняется первая буква {fio})-------->{hash_object.hexdigest()}")
time4 = int(round(time.time() * 1000)) - t  # Данные для таблицы
# ---------------------------------------------
ride_my_file = list(ride_my_file.decode('utf-8'))
ride_my_file[0] = chr(ord(ride_my_file[0]) ^ 1)
ride_my_file = ''.join(ride_my_file)
hash_object = hashlib.md5(ride_my_file.encode('utf-8'))
fio5 = fio  # Данные для таблицы
hesh5 = hash_object.hexdigest()  # Данные для таблицы
print(f"Хеш с заменной бита файла(при замене меняется первая буква {fio})-->{hash_object.hexdigest()}")
time5 = int(round(time.time() * 1000)) - t  # Данные для таблицы
# ---------------------------------------------
print(time.time() * 1000 - t)
with open("time_file.csv", 'w', newline='') as csvfile:
    writer = csv.writer(csvfile, delimiter=";")
    writer.writerow(["Time in milliseconds", "hashed", "hash", 'file type'])
    writer.writerow([time1, fio1, hesh1, 'string'])
    writer.writerow([time2, fio2, hesh2, 'file'])
    writer.writerow([time3, fio3, hesh3, 'string'])
    writer.writerow([time4, fio4, hesh4, 'string'])
    writer.writerow([time5, fio5, hesh5, 'file'])
    writer.writerow([])
    writer.writerow(['total time->', int(round(time.time() * 1000)) - t])
